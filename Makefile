all : scut tags

scut : scut.c
	gcc -Wall -pedantic scut.c -o scut  -O0 -g

clean:
	-rm scut

tags:
	etags scut.c
