#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h> // abort
#include <unistd.h> // getopt
#include <string.h> // memset
#include <ctype.h> // isprint

static bool trace = false;

#define N_PORTS 2

#define MASK2 03
#define MASK3 07
#define MASK4 017
#define MASK5 037
#define MASK6 077
#define MASK9 0777
#define MASK12 07777
#define MASK18 0777777

#define MASK_IC MASK12
#define MASK_REGNO MASK4
#define MASK_REG MASK18

typedef unsigned int uint;
typedef uint8_t  word1;
typedef uint8_t  word2;
typedef uint8_t  word3;
typedef uint8_t  word4;
typedef uint8_t  word5;
typedef uint8_t  word6;
typedef uint16_t word9;
typedef uint16_t word12;
typedef uint32_t word18;
typedef uint32_t word24;

typedef enum {
  rnA  = 000,  // 18 bits
  rnB  = 001,  // 18 bits
  rnE  = 002,  // 18 bits
  rnL  = 003,  // 18 bits
  rnH  = 004,  // 18 bits
  rnZ  = 005,  //  4 bits
  rnT  = 006,  //  9 bits
  rnS  = 007,  //  9 bits
  rnR0 = 010,  // 18 bits
  rnR1 = 011,  // 18 bits
  rnR2 = 012,  // 18 bits
  rnR3 = 013,  // 18 bits
  rnR4 = 014,  // 18 bits
  rnR5 = 015,  // 18 bits
  rnR6 = 016,  // 18 bits
  rnR7 = 017   // 18 bits
} regName_e;

typedef enum {
  sRA = 0,
  sR = 1,
  sCIP = 2,
  sIP = 3,
  sCON = 4
} sigNames_e;

typedef struct threadState_s {
  // Thread id
  uint threadNo;
  uint portNo;

  // Registers
  word12 rIC;
  word1  rxE;
  word1  rxZ;
  word9  rT;
  word9  rD;
  word18 rC;
  word5  rCMD;
  word18 rRegs[16];
  word18 rMask[16];
  word18 rCmp[16];
  bool signals[5]; // sigNames_e

  // Flags
  bool died;
  bool die;
  bool illOp;
  bool outOfRange;

  // Wait states
  bool waitOn;
  bool waitOff;
  bool waitOnCIP;
  word3 waitSigNo;
  word12 conditionalTraAddr;

  // RTL
  bool newDie;
  bool newCMD;

} threadState_t;

static threadState_t threadStates[N_PORTS];
static word24 ROM [4096];

// SCU port
typedef struct scuPort_s {
   // In
   word18 address;
   word18 data;
   word18 extData;
   word5 cmd;
   word4 zone;
   bool cmdReq;

   // Out
   word18 resultLow;
   word18 resultHigh;
   bool resultAvailable;
   bool ready;
   bool CIP;
   word2 IA;
   bool intPending_connect;
} scuPort_t;

static scuPort_t scuPorts[N_PORTS];

static bool regMod [16];
// static bool regLoad [16];
  
static void scutInit (void) {
#if 0
  for (uint p = 0; p < N_PORTS; p ++) {
    scuPorts[p].cmdReq = false;
    for (uint s = 0; s < 5; s ++) {
      scuPorts[p].signals[p] = false;
    }
  }
#endif
  memset (threadStates, 0, sizeof (threadStates));
  for (uint tNo = 0; tNo < N_PORTS; tNo ++) {
    threadStates[tNo].threadNo = tNo;
    threadStates[tNo].portNo = tNo;
  }
  memset (scuPorts, 0, sizeof (scuPorts));
  memset (regMod, 0, sizeof (regMod));
  regMod [rnA] = true;
  regMod [rnB] = true;
  regMod [rnE] = true;
  regMod [rnR0] = true;
  regMod [rnR1] = true;
  regMod [rnR2] = true;
  regMod [rnR3] = true;
//   memset (regLoad, 0, sizeof (regLoad));
//   regLoad [rnA] = true;
//   regLoad [rnB] = true;
//   regLoad [rnE] = true;
//   regLoad [rnZ] = true;
//   regLoad [rnT] = true;
//   regLoad [rnC] = true;
//   regLoad [rnR0] = true;
//   regLoad [rnR1] = true;
//   regLoad [rnR2] = true;
//   regLoad [rnR3] = true;
}

static void scutReset (void) {
  threadStates[0].rIC = 0;
  threadStates[1].rIC = 1;
}

static void outOfRangeFault (threadState_t * tsp) {
  tsp->outOfRange = true;
  threadStates[0].newDie = true;
  threadStates[1].newDie = true;
}
  
static void illOpFault (threadState_t * tsp) {
  tsp->illOp = true;
  threadStates[0].newDie = true;
  threadStates[1].newDie = true;
}
  
static inline uint otherThread (uint threadNo) {
  return 1 - threadNo;
}


#if 0
static void updateReg (threadState_t * tsp, word4 regNo) {
  switch (regNo) {
    case rnR0:
    case rnR1:
    case rnR2:
    case rnR3:
      threadStates[otherThread (tsp->threadNo)].rRegs[regNo + 4] = tsp->rRegs[regNo];
      break;
    default:
      break;
  }
}
#endif


static void exec (threadState_t * tsp) {
  word24 ins = ROM [tsp->rIC];
  if (trace) {
    fprintf (stderr, "%o %04o:%08o\n", tsp->threadNo, tsp->rIC, ins);
  }
  tsp->rIC = (tsp->rIC + 1) & MASK_IC;
  word6 op = (ins >> 18) & MASK6;
  switch (op) {


    case 000: {   // DIE
      tsp->rD = ins & MASK9;
      threadStates[0].newDie = true;
      threadStates[1].newDie = true;
    }
    break;


    case 001: {  // NOP
    }
    break;


    case 002: {  // CMD
      tsp->rCMD = ins & MASK5;
      tsp->newCMD = true;
    }
    break;

    
    case 003: {  // INC
      word4 regNo = ins & MASK_REGNO;
      if (! regMod[regNo]) {
        illOpFault (tsp);
      }
      tsp->rRegs[regNo] = (tsp->rRegs[regNo] + 1u) & MASK_REG;
      //updateReg (tsp, regNo);
    }
    break;


    case 004: {  // DEC
      word4 regNo = ins & MASK_REGNO;
      if (! regMod[regNo]) {
        illOpFault (tsp);
      }
      tsp->rRegs[regNo] = (tsp->rRegs[regNo] - 1u) & MASK_REG;
      //updateReg (tsp, regNo);
    }
    break;



    case 005: {  // transfers 1
      word3 traOp = (ins >> 15) & MASK3;
      word12 traAddr = ins & MASK_IC;

      switch (traOp) {

        case 00: {  // TRA
          tsp->rIC = traAddr;
        }
        break;

        case 01: {  // TON
          word3 sigNo = (ins >> 12) & MASK3;
          if (sigNo > 4)
            outOfRangeFault (tsp);
#if 0
          if (tsp->rC == 0) {
            if (scuPorts[tsp->portNo].signals[sigNo]) {
              tsp->rIC = traAddr;
            }
          } else {
            tsp->waitOn = true;
            tsp->waitSigNo = sigNo;
            tsp->conditionalTraAddr = traAddr;
          }
#else
          tsp->waitOn = true;
          tsp->waitSigNo = sigNo;
          tsp->conditionalTraAddr = traAddr;
#endif
        }
        break;


        case 02: {  // TOFF
          word3 sigNo = (ins >> 12) & MASK3;
          if (sigNo > 4)
            outOfRangeFault (tsp);
#if 0
          if (tsp->rC == 0) {
            if (! scuPorts[tsp->portNo].signals[sigNo]) {
              tsp->rIC = traAddr;
            }
          } else {
            tsp->waitOff = true;
            tsp->waitSigNo = sigNo;
            tsp->conditionalTraAddr = traAddr;
          }
#else
          tsp->waitOff = true;
          tsp->waitSigNo = sigNo;
          tsp->conditionalTraAddr = traAddr;
#endif
        }
        break;

        case 03: {  // TIA
          word2 IA = (ins >> 12) & MASK2;
          if (IA == (scuPorts[tsp->portNo].IA & MASK2)) {
            tsp->rIC = traAddr;
          }
        }
        break;



      } // switch (traOp)
    } // case Transfers 1
    break;

    case 06: {  // Transfers 2
      word3 traOp = (ins >> 15) & MASK3;
      word3 regNo = (ins >> 12) & MASK3;
      word12 traAddr = ins & MASK_IC;
      switch (traOp) {
        case 00: {  // TrEQ
          if ((tsp->rRegs[regNo] & tsp->rMask[regNo]) == tsp->rCmp[regNo]) {
            tsp->rIC = traAddr;
          }
        }
        break;

        case 01: {  // TrNE
          if ((tsp->rRegs[regNo] & tsp->rMask[regNo]) != tsp->rCmp[regNo]) {
            tsp->rIC = traAddr;
          }
        }
        break;

        case 02: {  // TrZE
          if ((tsp->rRegs[regNo] & tsp->rMask[regNo]) == 0) {
            tsp->rIC = traAddr;
          }
        }
        break;

        case MASK2: {  // TrNZ
          if ((tsp->rRegs[regNo] & tsp->rMask[regNo]) != 0) {
            tsp->rIC = traAddr;
          }
        }
        break;
      } // switch (traOp)
    } // case Transfers 2
    break;

    case 007: {  // TIDX
      word4 regNo = (ins >> 12) & MASK_REGNO;
      word12 limit = ins & MASK_IC;
      if (tsp->rRegs[regNo] > limit) {
        outOfRangeFault (tsp);
      }
      tsp->rIC = tsp->rIC + tsp->rRegs[regNo];
    }
    break;


    case 010: {  // LDC
      tsp->rC = ins & MASK_REG;
    }
    break;


    case 020:    // LDA
    case 021:    // LDB
    case 022:    // LDE
    case 025:    // LDZ
    case 026:    // LDT
    case 030:    // LD0
    case 031:    // LD1
    case 032:    // LD2
    case 033: {  // LD3
      word4 regNo = op & MASK_REGNO;
      tsp->rRegs[regNo] = ins & MASK_REG;
      //updateReg (tsp, regNo);
    }
    break;


    case 040:    // LDAM
    case 041:    // LDBM
    case 042:    // LDEM
    case 043:    // LDLM
    case 044:    // LDHM
    case 047: {  // LDSM
      word4 regNo = op & MASK3;
      tsp->rMask[regNo] = ins & MASK_REG;
    }
    break;


    case 060:    // LD0M
    case 061:    // LD1M
    case 062:    // LD2M
    case 063:    // LD3M
    case 064:    // LD4M
    case 065:    // LD5M
    case 066:    // LD6M
    case 067: {  // LD7M
      word4 regNo = (op & MASK3) + 010;
      tsp->rMask[regNo] = ins & MASK_REG;
    }
    break;


    case 050:    // LDAC
    case 051:    // LDBC
    case 052:    // LDEC
    case 053:    // LDLC
    case 054:    // LDHC
    case 057: {  // LDSC
      word4 regNo = op & MASK3;
      tsp->rCmp[regNo] = ins & MASK_REG;
    }
    break;


    case 070:    // LD0C
    case 071:    // LD1C
    case 072:    // LD2C
    case 073:    // LD3C
    case 074:    // LD4C
    case 075:    // LD5C
    case 076:    // LD6C
    case 077: {  // LD7C
      word4 regNo = (op & MASK3) + 010;
      tsp->rCmp[regNo] = ins & MASK_REG;
    }
    break;


    default:
      illOpFault (tsp);
      break;

  } // switch (op)
} // exec


static void scutCycleHigh (threadState_t * tsp) {
  if (tsp->died)
    return;

  if (tsp->die) {
    tsp->died = true;
    return;
  }
  if (tsp->illOp) {
    tsp->died = true;
    return;
  }
  if (tsp->outOfRange) {
    tsp->died = true;
    return;
  }
  
  // TRON/TROF wait states
  if (tsp->waitOn) {
    if (tsp->signals[tsp->waitSigNo]) {
      tsp->waitOn = false;
      tsp->rIC = tsp->conditionalTraAddr;
    }
  } // if wait on
  if (tsp->waitOff) {
    if (! tsp->signals[tsp->waitSigNo]) {
      tsp->waitOff = false;
      tsp->rIC = tsp->conditionalTraAddr;
    }
  } // if wait off
  if (tsp->waitOn || tsp->waitOff) {
    if (tsp->rC)
      tsp->rC = (tsp->rC - 1);
    if (tsp->rC)
      return;
    // Timeout
    tsp->waitOn = tsp->waitOff = false;  
  } // if waitOn or waitOff

  // CMD wait
  if (tsp->waitOnCIP) {
    if (tsp->signals[sCIP]) {
      scuPorts[tsp->portNo].cmdReq = false;
    } else {
      tsp->rC = (tsp->rC + 1) & MASK_REG;
      return;
   }
  } // waitOnCIP

  exec (tsp);
}


static void scutCycleLow (threadState_t * tsp) {
   // Set R4-R7
   threadState_t * other = & threadStates[otherThread(tsp->portNo)];
   tsp->rRegs[rnR4] = other->rRegs[rnR0];
   tsp->rRegs[rnR5] = other->rRegs[rnR1];
   tsp->rRegs[rnR6] = other->rRegs[rnR2];
   tsp->rRegs[rnR7] = other->rRegs[rnR3];

   // The port this thread is testing
   scuPort_t * port = & scuPorts[tsp->portNo];

   // From the port
   // Set S
   tsp->rRegs[rnS] =
     (port->resultAvailable    ? 040 : 0) |
     (port->ready              ? 020 : 0) |
     (port->CIP                ? 010 : 0) |
     (port->intPending_connect ? 004 : 0) |
     (port->IA & 03);

   // Set signals
   tsp->signals[sRA]  = port->resultAvailable;
   tsp->signals[sR]   = port->ready;
   tsp->signals[sCIP] = port->CIP;
   tsp->signals[sCON] = port->intPending_connect;

   // Set registers
   tsp->rRegs[rnL] = port->resultLow;
   tsp->rRegs[rnH] = port->resultHigh;

   // To the port
   port->address = tsp->rRegs[rnA];
   port->data = tsp->rRegs[rnB];
   port->extData = tsp->rRegs[rnE];
   port->zone = tsp->rRegs[rnZ];
   // Set CMD
   if (tsp->newCMD) {
    tsp->newCMD = false;
    scuPorts[tsp->portNo].cmd = tsp->rCMD;
    scuPorts[tsp->portNo].cmdReq = true;
    tsp->waitOnCIP = true;
    tsp->rC = 0;
   }
   // Update die
   tsp->die = tsp->newDie;
}


static void loadROM (void) {
  ROM[  0] = 005000002; //   0: TRA 2
  ROM[  1] = 005000004; //   1: TRA 4
  ROM[  2] = 030123456; //   2: LD0 0123456
  ROM[  3] = 000000001; //   3: DIE 1
  ROM[  4] = 030654321; //   4: LD0 0654321
  ROM[  5] = 000000002; //   5: DIE 2
}

static void scuCycleHigh (scuPort_t * spp) {
}


static void scuCycleLow (scuPort_t * spp) {
}


int main (int argc, char * argv []) {

  int c;
  while ((c = getopt (argc, argv, "t")) != -1) {
    switch (c) {
      case 't':
        trace = true;
        break;
      case '?':
        if (isprint (optopt))
          fprintf (stderr, "Unknown option '-%c'.\n", optopt);
        else
          fprintf (stderr, "Unknown option '-\\%o'.\n", optopt);
        return 1;
      default:
        abort ();
    }
  }
  scutInit ();
  scutReset ();
  loadROM ();
  while (1) {
    if (threadStates[0].died && threadStates[1].died)
      break;
    scutCycleHigh (& threadStates[0]);
    scutCycleHigh (& threadStates[1]);
    scuCycleHigh (& scuPorts[0]);
    scuCycleHigh (& scuPorts[1]);
    scutCycleLow (& threadStates[0]);
    scutCycleLow (& threadStates[1]);
    scuCycleLow (& scuPorts[0]);
    scuCycleLow (& scuPorts[1]);
  }

  printf ("Thread: 0 R0: %06o D: %03o\n", threadStates[0].rRegs[rnR0], threadStates[0].rD);
  printf ("Thread: 1 R0: %06o D: %03o\n", threadStates[1].rRegs[rnR0], threadStates[1].rD);
}
